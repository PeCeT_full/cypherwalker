# Cypher Walker
# Copyright (C) 2015-2021 PeCeT_full
# This file is distributed under the same license as the CypherWalker package.
# PeCeT_full <me@pecetfull.pl>, 2015-2021.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-07-13 15:39+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: dump/CypherWalkerMain.cpp:118
msgid "&About..."
msgstr ""

#: dump/OptionsDialog.cpp:73
msgid "&Apply"
msgstr ""

#: dump/OptionsDialog.cpp:64
msgid "&Change"
msgstr ""

#: dump/HiScoresDialog.cpp:89
msgid "&Erase all"
msgstr ""

#: dump/CypherWalkerMain.cpp:114
msgid "&High scores"
msgstr ""

#: dump/CypherWalkerMain.cpp:116
msgid "&Options"
msgstr ""

#: dump/CypherWalkerMain.cpp:111
msgid "&Play"
msgstr ""

#: dump/CypherWalkerMain.cpp:121
msgid "&Quit"
msgstr ""

#: dump/CypherWalkerMain.cpp:173
msgid ""
"A dynamic logic game designed for both vintage and modern computers.\n"
"\n"
"Build info: "
msgstr ""

#: dump/OptionsDialog.cpp:115
msgid ""
"Another language has just been selected. To reflect all the changes, please "
"restart the application."
msgstr ""

#: dump/wxTranslationHelper.cpp:73
msgid "Array of language names and identifiers should have the same size."
msgstr ""

#: dump/EnterNameDialog.cpp:39 dump/OptionsDialog.cpp:71
msgid "Cancel"
msgstr ""

#: dump/CypherWalkerMain.cpp:289
#, c-format
msgid "Congratulations! You've gained %d points in total."
msgstr ""

#: dump/EnterNameDialog.cpp:30
#, c-format
msgid ""
"Congratulations! You've made it into the Top 5 with %d points you've got!\n"
"Please enter your name: "
msgstr ""

#: dump/OptionsDialog.cpp:62 dump/OptionsDialog.cpp:95
msgid "Current language: "
msgstr ""

#: dump/CypherWalkerMain.cpp:86 dump/CypherWalkerMain.cpp:170
msgid "Cypher Walker"
msgstr ""

#: dump/CypherWalkerMain.h:37
msgid "Cypher Walker - Score: "
msgstr ""

#: dump/CypherWalkerMain.cpp:104
msgid "Down (&K)"
msgstr ""

#: dump/EnterNameDialog.cpp:68
msgid "Error"
msgstr ""

#: dump/OptionsDialog.cpp:39
msgid "Game options"
msgstr ""

#: dump/CypherWalkerMain.cpp:289 dump/EnterNameDialog.cpp:26
msgid "Game over"
msgstr ""

#: dump/CypherWalkerMain.cpp:221
msgid "Generating and initialising the board, please wait..."
msgstr ""

#: dump/HiScoresDialog.cpp:39
msgid "Hall of Fame"
msgstr ""

#: dump/HiScoresDialog.cpp:37
msgid "High scores"
msgstr ""

#: dump/CypherWalkerMain.cpp:174
msgid "http://www.pecetfull.pl"
msgstr ""

#: dump/wxTranslationHelper.cpp:74
msgid "Language"
msgstr ""

#: dump/CypherWalkerMain.cpp:97
msgid "Left (&J)"
msgstr ""

#: dump/OptionsDialog.cpp:50
msgid "Lost game sound:"
msgstr ""

#: dump/OptionsDialog.cpp:46
msgid "Making a move sound:"
msgstr ""

#: dump/OptionsDialog.cpp:115
msgid "Message"
msgstr ""

#: dump/OptionsDialog.cpp:42
msgid "New game sound:"
msgstr ""

#: dump/OptionsDialog.cpp:54
msgid "New record sound:"
msgstr ""

#: dump/EnterNameDialog.cpp:36 dump/HiScoresDialog.cpp:86
#: dump/OptionsDialog.cpp:68
msgid "OK"
msgstr ""

#: dump/CypherWalkerMain.cpp:100
msgid "Right (&L)"
msgstr ""

#: dump/wxTranslationHelper.cpp:74
msgid "Select your language."
msgstr ""

#: dump/CypherWalkerMain.cpp:175
msgid ""
"This program is published under The MIT License. For more information, "
"please refer to the Licence.txt file included with the application."
msgstr ""

#: dump/CypherWalkerMain.cpp:93
msgid "Up (&I)"
msgstr ""

#: dump/EnterNameDialog.cpp:68
msgid ""
"Your name consists of at least one vertical bar. Please remove them and try "
"again."
msgstr ""

#: OK (with hotkey)
msgid "&OK"
msgstr ""

#: Cancel (with hotkey)
msgid "&Cancel"
msgstr ""

#: About
msgid "About "
msgstr ""

#: License
msgid "License"
msgstr ""

#: Translators
msgid "Translators"
msgstr ""

#: Bulgarian
msgid "Bulgarian"
msgstr ""

#: Czech
msgid "Czech"
msgstr ""

#: English
msgid "English"
msgstr ""

#: English (Canada)
msgid "English (Canada)"
msgstr ""

#: Estonian
msgid "Estonian"
msgstr ""

#: German
msgid "German"
msgstr ""

#: Polish
msgid "Polish"
msgstr ""

#: Slovak
msgid "Slovak"
msgstr ""
