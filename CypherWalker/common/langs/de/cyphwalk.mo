��    4      �  G   \      x  	   y     �     �     �  
   �     �     �     �     �     �  R   �       d   %  B   �  	   �     �  2   �  b        t     �     �     �  	   �     �     �     �     �     �  	   �  5   �     2     9     F     R  	   [     e     m     ~     �     �     �     �     �  
   �     �     �  �   �     z     �  R   �     �  �  �     {
  	   �
  
   �
     �
     �
     �
     �
  	   �
     �
     �
  g   �
     K  x   Q  I   �  
     	     >   )  �   h     �               2     >     J     S     e     l     u     �  ;   �     �     �     �     �  
   �               .  	   N     X     r     �     �     �     �  
   �  �   �  
   O  	   Z  p   d     �     3   2           !      *   '      	           /                     ,   "   (   1              0                  4   .      +          )          &                        #                             
               $                   %         -    &About... &Apply &Cancel &Change &Erase all &High scores &OK &Options &Play &Quit A dynamic logic game designed for both vintage and modern computers.

Build info:  About  Another language has just been selected. To reflect all the changes, please restart the application. Array of language names and identifiers should have the same size. Bulgarian Cancel Congratulations! You've gained %d points in total. Congratulations! You've made it into the Top 5 with %d points you've got!
Please enter your name:  Current language:  Cypher Walker Cypher Walker - Score:  Czech Down (&K) English English (Canada) Error Estonian Game options Game over Generating and initialising the board, please wait... German Hall of Fame High scores Language Left (&J) License Lost game sound: Making a move sound: Message New game sound: New record sound: OK Polish Right (&L) Select your language. Slovak This program is published under The MIT License. For more information, please refer to the Licence.txt file included with the application. Translators Up (&I) Your name consists of at least one vertical bar. Please remove them and try again. http://www.pecetfull.pl Project-Id-Version: Cypher Walker 1.0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-13 18:08+0200
PO-Revision-Date: 2015-07-15 17:58+0200
Last-Translator: PeCeT_full <me@pecetfull.pl>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=windows-1250
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
Plural-Forms: nplurals=2; plural=(n != 1);
Language: de
 &�ber... &Anwenden &Abbrechen &�ndern Alles &l�schen Beste &Punktzahlen &OK &Optionen &Spielen &Beenden Ein dynamisches Logikspiel sowohl f�r altmodischen als auch modernen Computer entwickelt.

Build-Info:  �ber  Eine andere Sprache hat gerade ausgew�hlt worden. Um alle �nderungen zu �bernehmen, starten Sie bitte die Anwendung neu. Das Array von Sprachennamen und Kennungen sollte die gleiche Gr��e haben. Bulgarisch Abbrechen Herzlichen Gl�ckwunsch! Sie haben insgesamt %d Punkte erlangt. Herzlichen Gl�ckwunsch! Sie haben es unter die besten 5 mit %d Punkte, die Sie erzielt haben, geschafft!
Bitte geben Sie Ihren Namen ein:  Aktuelle Sprache:  Cypher Walker Cypher Walker - Punktestand:  Tschechisch Runter (&K) Englisch Englisch (Kanada) Fehler Estnisch Spieloptionen Ende des Spiels Das Brett wird generiert und initialisiert, bitte warten... Deutsch Ruhmeshalle Beste Punktzahlen Sprache Links (&J) Lizenz Klang eines verlorenen Spiels: Klang eines gemachten Schritts: Nachricht Klang eines neuen Spiels: Klang eines neuen Rekords: OK Polnisch Rechts (&L) W�hlen Sie Ihre Sprache aus. Slowakisch Dieses Programm steht unter der MIT-Lizenz. Weitere Angaben hierzu finden Sie unter Licence.txt-Datei in der Anwendung enthalten. �bersetzer Hoch (&I) Ihr Name besteht aus mindestens einen senkrechten Strich. Bitte entfernen Sie ihnen und versuchen Sie es erneut. http://www.pecetfull.pl/de 