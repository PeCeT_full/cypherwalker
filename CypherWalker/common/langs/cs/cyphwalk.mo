��    4      �  G   \      x  	   y     �     �     �  
   �     �     �     �     �     �  R   �       d   %  B   �  	   �     �  2   �  b        t     �     �     �  	   �     �     �     �     �     �  	   �  5   �     2     9     F     R  	   [     e     m     ~     �     �     �     �     �  
   �     �     �  �   �     z     �  R   �     �  �  �  	   �
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
  U   �
     J  [   Q  @   �     �     �  +     \   -     �  	   �     �     �  	   �  
   �     �     �  
   �     �  	     4        G  	   O     Y     h  
   n     y     �     �     �     �     �     �     �     �     �     �  y        ~     �  X   �     �     3   2           !      *   '      	           /                     ,   "   (   1              0                  4   .      +          )          &                        #                             
               $                   %         -    &About... &Apply &Cancel &Change &Erase all &High scores &OK &Options &Play &Quit A dynamic logic game designed for both vintage and modern computers.

Build info:  About  Another language has just been selected. To reflect all the changes, please restart the application. Array of language names and identifiers should have the same size. Bulgarian Cancel Congratulations! You've gained %d points in total. Congratulations! You've made it into the Top 5 with %d points you've got!
Please enter your name:  Current language:  Cypher Walker Cypher Walker - Score:  Czech Down (&K) English English (Canada) Error Estonian Game options Game over Generating and initialising the board, please wait... German Hall of Fame High scores Language Left (&J) License Lost game sound: Making a move sound: Message New game sound: New record sound: OK Polish Right (&L) Select your language. Slovak This program is published under The MIT License. For more information, please refer to the Licence.txt file included with the application. Translators Up (&I) Your name consists of at least one vertical bar. Please remove them and try again. http://www.pecetfull.pl Project-Id-Version: Cypher Walker 1.0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-13 15:39+0200
PO-Revision-Date: 2019-08-25 17:53+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=windows-1250
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
Last-Translator: PeCeT_full <me@pecetfull.pl>
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
Language: cs
 O &h�e... &Pou��t &Storno &Zm�nit &Vymazat v�e &Nejvy��� sk�re &OK &Mo�nosti &Zahr�t &Ukon�it Dynamick� logick� hra navr�en� pro star� a modern� po��ta�e.

Informace o sestaven�:  O h�e  Pr�v� byl vybr�n jin� jazyk. Chcete-li zobrazit v�echny zm�ny, restartujte pros�m aplikaci. Pole n�zv� jazyk� a identifik�tor� by m�ly m�t stejnou velikost. Bulhar�tina Storno Gratuluji! Z�skali jste celkem %d bod(y/�). Gratuluji! Dostali jste se do nejlep�� p�tky s %d z�skan�mi body!
Zadejte pros�m sv� jm�no:  Aktu�ln� jazyk:  Cifrochod Cifrochod - Sk�re:  �e�tina Dol� (&K) Angli�tina Angli�tina (Kanada) Chyba Eston�tina Mo�nosti hry Konec hry Generov�n� a inicializace desky, po�kejte, pros�m... N�m�ina S�� sl�vy Nejvy��� sk�re Jazyk Vlevo (&J) Licence Zvuk prohran� hry: Zvuk pohybu: Zpr�va Zvuk nov� hry: Zvuk nov�ho rekordu: OK Pol�tina Vpravo (&L) Vyberte sv�j jazyk. Sloven�tina Tento program je publikov�n pod licenc� MIT. Dal�� informace naleznete v souboru Licence.txt, kter� je sou��st� aplikace. P�ekladatel� Nahoru (&I) Va�e jm�no se skl�d� z alespo� jedn� svisl� ��ry. Pros�m odstra�te je a zkuste to znovu. http://www.pecetfull.pl 