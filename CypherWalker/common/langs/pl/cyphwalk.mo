��    4      �  G   \      x  	   y     �     �     �  
   �     �     �     �     �     �  R   �       d   %  B   �  	   �     �  2   �  b        t     �     �     �  	   �     �     �     �     �     �  	   �  5   �     2     9     F     R  	   [     e     m     ~     �     �     �     �     �  
   �     �     �  �   �     z     �  R   �     �  �  �  
   �
  	   �
     �
     �
     �
     �
     �
                 x        �  n   �  R   	  	   \     f  +   m  h   �       
             1     8  	   A     K     ^     c  	   l  
   v  6   �  	   �     �     �     �  	   �     �     �       	   (     2     D     [     ^  
   e     p     �    �     �  	   �  f   �          3   2           !      *   '      	           /                     ,   "   (   1              0                  4   .      +          )          &                        #                             
               $                   %         -    &About... &Apply &Cancel &Change &Erase all &High scores &OK &Options &Play &Quit A dynamic logic game designed for both vintage and modern computers.

Build info:  About  Another language has just been selected. To reflect all the changes, please restart the application. Array of language names and identifiers should have the same size. Bulgarian Cancel Congratulations! You've gained %d points in total. Congratulations! You've made it into the Top 5 with %d points you've got!
Please enter your name:  Current language:  Cypher Walker Cypher Walker - Score:  Czech Down (&K) English English (Canada) Error Estonian Game options Game over Generating and initialising the board, please wait... German Hall of Fame High scores Language Left (&J) License Lost game sound: Making a move sound: Message New game sound: New record sound: OK Polish Right (&L) Select your language. Slovak This program is published under The MIT License. For more information, please refer to the Licence.txt file included with the application. Translators Up (&I) Your name consists of at least one vertical bar. Please remove them and try again. http://www.pecetfull.pl Project-Id-Version: Cypher Walker 1.0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-13 15:52+0200
PO-Revision-Date: 2021-01-30 11:21+0100
Last-Translator: PeCeT_full <me@pecetfull.pl>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=windows-1250
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: pl
 O &grze... &Zastosuj &Anuluj Z&mie� &Wyczy�� wszystko &Najlepsze wyniki &OK &Opcje &Zagraj &Wyj�cie Dynamiczna gra logiczna zaprojektowana zar�wno dla zabytkowych jak i nowoczesnych komputer�w.

Informacje o kompilacji:  O grze  W�a�nie zosta� wybrany inny j�zyk. Aby wszelkie zmiany zosta�y odzwierciedlone, prosz� zrestartowa� aplikacj�. Tablica nazw j�zyk�w powinna by� tego samego rozmiaru, co tablica identyfikator�w. Bu�garski Anuluj Gratulacje! Uzyska�e� razem %d punkt(y/�w). Gratulacje! Dosta�e� si� do najlepszej pi�tki z %d punktami, kt�re zdoby�e�!
Prosz� wpisa� swoj� nazw�:  Obecny j�zyk:  Cyfro�azik Cyfro�azik - Wynik:  Czeski D� (&K) Angielski Angielski (Kanada) B��d Esto�ski Opcje gry Koniec gry Generowanie oraz inicjowanie planszy, prosz� czeka�... Niemiecki Galeria s�aw Najlepsze wyniki J�zyk Lewo (&J) Licencja D�wi�k przegranej gry: D�wi�k wykonania ruchu: Komunikat D�wi�k nowej gry: D�wi�k nowego rekordu: OK Polski Prawo (&L) Wybierz sw�j j�zyk. S�owacki Ten program jest publikowany na zasadach licencji MIT. Wi�cej informacji mo�na uzyska� zapoznaj�c si� z plikiem Licence.txt do��czonym do aplikacji (polskie t�umaczenie licencji MIT: http://blaszyk-jarosinski.pl/wp-content/uploads/2008/05/licencja-mit-tlumaczenie.pdf). T�umacze G�ra (&I) Twoja nazwa sk�ada si� z co najmniej jednej kreski pionowej. Prosz� je usun�� i spr�bowa� jeszcze raz. http://www.pecetfull.pl/pl 