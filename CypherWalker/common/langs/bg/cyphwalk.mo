��    4      �  G   \      x  	   y     �     �     �  
   �     �     �     �     �     �  R   �       d   %  B   �  	   �     �  2   �  b        t     �     �     �  	   �     �     �     �     �     �  	   �  5   �     2     9     F     R  	   [     e     m     ~     �     �     �     �     �  
   �     �     �  �   �     z     �  R   �     �  �  �     {
     �
     �
     �
     �
     �
     �
     �
     �
     �
  f   �
     L  \   U  Y   �  	          +     ~   H     �     �     �     �  	     	             *     1     :     J  9   Z     �     �     �     �  	   �     �     �     �  
   
          (     <     B  
   I     T     i  �   r  	   �  	     V        e     3   2           !      *   '      	           /                     ,   "   (   1              0                  4   .      +          )          &                        #                             
               $                   %         -    &About... &Apply &Cancel &Change &Erase all &High scores &OK &Options &Play &Quit A dynamic logic game designed for both vintage and modern computers.

Build info:  About  Another language has just been selected. To reflect all the changes, please restart the application. Array of language names and identifiers should have the same size. Bulgarian Cancel Congratulations! You've gained %d points in total. Congratulations! You've made it into the Top 5 with %d points you've got!
Please enter your name:  Current language:  Cypher Walker Cypher Walker - Score:  Czech Down (&K) English English (Canada) Error Estonian Game options Game over Generating and initialising the board, please wait... German Hall of Fame High scores Language Left (&J) License Lost game sound: Making a move sound: Message New game sound: New record sound: OK Polish Right (&L) Select your language. Slovak This program is published under The MIT License. For more information, please refer to the Licence.txt file included with the application. Translators Up (&I) Your name consists of at least one vertical bar. Please remove them and try again. http://www.pecetfull.pl Project-Id-Version: Cypher Walker 1.0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-13 15:39+0200
PO-Revision-Date: 2018-07-20 11:30+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=windows-1251
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
Last-Translator: PeCeT_full <me@pecetfull.pl>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: bg
 ������� &������� &������� &����� �&������ &������� ������ &���-������� ������ &����� &����� �&���� ��&��� ��������� ��������� ���� ����������� ����� �� �����, ���� � �� ������� ��������.

���������� �� ����:  �������  ���� �� �� ������ ���� ����. �� �� �������� ������ �������, ���� ������������� ������������. ����� �� ������� �� ������� � ����� �� ���������������� ������ �� ����� � ������� ������. ��������� ����� ������������! ��� ���������� ���� %d �����. ������������! ��� ������� �� ������� � ����������� �� ����� ���-����� � %d �����, ����� ����������!
���� �������� ������ ���:  ����� ����:  Cypher Walker Cypher Walker - ��������:  ����� ���� (&K) ��������� ��������� (������) ������ �������� ����� �� ������ ������ �������� ���������� � �������������� �� �������, ���� ���������... ������ ���� �� ������� ���-������ ��������� ���� ���� (&J) ������ ���� �� �������� ����: ���� �� ������� �� ��������: ���������� ���� �� ���� ����: ���� �� ��� ������: ����� ������ ����� (&L) �������� ����� ����. �������� ���� �������� � �������� �������� ��� ������� MIT. �� ������ ���������� ���� ��������� �� �� ������ Licence.txt ������� � ������������. ��������� ���� (&I) ������ ��� �� ������ �� ���� ���� ���������� �����. ���� ���������� �� � �������� ���. http://www.pecetfull.pl 