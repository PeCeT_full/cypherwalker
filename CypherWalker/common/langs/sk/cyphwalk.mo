��    4      �  G   \      x  	   y     �     �     �  
   �     �     �     �     �     �  R   �       d   %  B   �  	   �     �  2   �  b        t     �     �     �  	   �     �     �     �     �     �  	   �  5   �     2     9     F     R  	   [     e     m     ~     �     �     �     �     �  
   �     �     �  �   �     z     �  R   �     �  �  �  	   �
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
  S   �
     L  S   S  A   �  
   �     �  +   �  g   '     �     �     �     �  	   �  
   �     �     �  	   �       
     8        X  
   `     k     {  
   �     �     �     �     �     �     �     �     �     �       
     {   %     �  	   �  Y   �          3   2           !      *   '      	           /                     ,   "   (   1              0                  4   .      +          )          &                        #                             
               $                   %         -    &About... &Apply &Cancel &Change &Erase all &High scores &OK &Options &Play &Quit A dynamic logic game designed for both vintage and modern computers.

Build info:  About  Another language has just been selected. To reflect all the changes, please restart the application. Array of language names and identifiers should have the same size. Bulgarian Cancel Congratulations! You've gained %d points in total. Congratulations! You've made it into the Top 5 with %d points you've got!
Please enter your name:  Current language:  Cypher Walker Cypher Walker - Score:  Czech Down (&K) English English (Canada) Error Estonian Game options Game over Generating and initialising the board, please wait... German Hall of Fame High scores Language Left (&J) License Lost game sound: Making a move sound: Message New game sound: New record sound: OK Polish Right (&L) Select your language. Slovak This program is published under The MIT License. For more information, please refer to the Licence.txt file included with the application. Translators Up (&I) Your name consists of at least one vertical bar. Please remove them and try again. http://www.pecetfull.pl Project-Id-Version: Cypher Walker 1.0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-13 15:39+0200
PO-Revision-Date: 2018-07-20 13:13+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=windows-1250
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
Last-Translator: PeCeT_full <me@pecetfull.pl>
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
Language: sk
 O &hre... &Pou�i� &Zru�i� &Zmeni� &Vymaza� v�etko &Najvy��ie sk�re &OK &Mo�nosti &Zahra� &Skon�i� Dynamick� logick� hra navrhnut� pre star� a modern� po��ta�e.

Build - inform�cie:  O hre  Bol pr�ve vybran� in� jazyk. Aby odrazi� v�etky zmeny, pros�m, re�tartujte program. Pole n�zvov jazykov a identifik�torov mali by� rovnakej ve�kosti. Bulhar�ina Zru�i� Gratulujem! Z�skali ste spolu %d bod(y/ov). Gratulujem! Podarilo sa V�m dosta� do najlep��ch p� so %d z�skan�mi bodmi!
Pros�m, zadajte Va�e meno:  Aktu�lny jazyk:  Cypher Walker Cypher Walker - Sk�re:  �e�tina Dole (&K) Angli�tina Angli�tina (Kanada) Chyba Est�n�ina Mo�nosti hry Koniec hry Generovanie a inicializovanie dosky, po�kajte, pros�m... Nem�ina Sie� sl�vy Najvy��ie sk�re Jazyk V�avo (&J) Licencia Zvuk stratenej hry: Zvuk vykonania pohybu: Hl�senie Zvuk novej hry: Zvuk nov�ho rekordu: OK Po��tina Vpravo (&L) Vyberte svoj jazyk. Sloven�ina Tento program je zverejnen� pod licenciou MIT. �al�ie inform�cie n�jdete v s�bore Licence.txt, ktor� je s��as�ou aplik�cie. Prekladatelia Hore (&I) Va�e meno sa sklad� z najmenej jednej zvislej �iary. Pros�m, odstr��te je a sk�ste znova. http://www.pecetfull.pl 