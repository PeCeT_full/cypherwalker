��    4      �  G   \      x  	   y     �     �     �  
   �     �     �     �     �     �  R   �       d   %  B   �  	   �     �  2   �  b        t     �     �     �  	   �     �     �     �     �     �  	   �  5   �     2     9     F     R  	   [     e     m     ~     �     �     �     �     �  
   �     �     �  �   �     z     �  R   �     �  �  �  	   �
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
  d   �
     O  R   d  ?   �  	   �       )     e   1     �     �     �     �  	   �     �     �     �            	     /         P     V     e     w  
   |     �     �     �     �     �     �     �     �  
   �     �       n        �  	   �  Y   �     �     3   2           !      *   '      	           /                     ,   "   (   1              0                  4   .      +          )          &                        #                             
               $                   %         -    &About... &Apply &Cancel &Change &Erase all &High scores &OK &Options &Play &Quit A dynamic logic game designed for both vintage and modern computers.

Build info:  About  Another language has just been selected. To reflect all the changes, please restart the application. Array of language names and identifiers should have the same size. Bulgarian Cancel Congratulations! You've gained %d points in total. Congratulations! You've made it into the Top 5 with %d points you've got!
Please enter your name:  Current language:  Cypher Walker Cypher Walker - Score:  Czech Down (&K) English English (Canada) Error Estonian Game options Game over Generating and initialising the board, please wait... German Hall of Fame High scores Language Left (&J) License Lost game sound: Making a move sound: Message New game sound: New record sound: OK Polish Right (&L) Select your language. Slovak This program is published under The MIT License. For more information, please refer to the Licence.txt file included with the application. Translators Up (&I) Your name consists of at least one vertical bar. Please remove them and try again. http://www.pecetfull.pl Project-Id-Version: Cypher Walker 1.0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-13 15:39+0200
PO-Revision-Date: 2021-01-30 11:21+0100
Language-Team: Mojito & Matuzka
MIME-Version: 1.0
Content-Type: text/plain; charset=windows-1257
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
Last-Translator: PeCeT_full <me@pecetfull.pl>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: et
 &Teave... &Rakenda &Loobu &Muuda &T�hjenda k�ik &Parimad tulemused &OK &Suvandid &M�ngi &V�lju D�naamiline loogikam�ng, mis on m�eldud nii vanadele kui ka t�nap�evastele arvutitele.

Ehitusj�rk:  Teave m�ngu kohta -  Teine keel on just valitud. K�igi muudatuste kajastamiseks taask�ivitage rakendus. Keelenimede ja identifikaatorite massiiv peaks olema sama suur. Bulgaaria Loobu Palju �nne! Olete kokku saanud %d punkti. Palju �nne! Olete p��senud 5 parema hulka %d punktidega, mis te teenisite!
Palun sisestage oma nimi:  Praegune keel:  Cypher Walker Cypher Walker - Punktiseis:  T�ehhi Alla (&K) Inglise Inglise (Kanada) Viga Eesti M�ngu suvandid M�ng l�bi Genereerin ja algv��rtustan laud, palun oota... Saksa Kuulsuste hall Parimad tulemused Keel Vasak (&J) Litsents Kaotatud m�ngu heli: Heli liigutuse tegemisel: Teade Uus m�ngu heli: Uus rekordi heli: OK Poola Parem (&L) Valige oma keel. Slovaki See programm on avaldatud MIT-litsentsi all. Lisateavet leiate rakendusega kaasas olevast failist License.txt. T�lkijad �les (&I) Teie nimi koosneb v�hemalt �hest vertikaalsest t�hest. Eemaldage need ja proovige uuesti. http://www.pecetfull.pl 