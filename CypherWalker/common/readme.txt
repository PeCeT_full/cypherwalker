Cypher Walker 1.0.1
Author: PeCeT_full
Release date: 30th January 2021
Website: http://www.pecetfull.pl
Copyright (c) by PeCeT_full 2015-2021. Cypher Walker is published under The MIT License. For more information, please refer to Licence.txt included with the application.

If there are any problems or doubts, please contact me.

----------------
Game description
----------------

Cypher Walker is a dynamic puzzle game designed for both vintage and modern computers (as an experiment) where your task is to fill the board consisting of randomly generated digits (from 1 to 9) with as many zeros as you simply can. It was written in C++ and uses wxWidgets GUI library. This game was created in order to celebrate the 20th Anniversary of Windows 95!

Minimal hardware and system requirements: 133 MHz or faster processor; 8 MB of available RAM; 1.2 MB of free hard disk space available where the game exists; Windows 95 or newer operating system.

------------
How to play?
------------

When you start the game, you are given a board which is of size 25x25. The first zero is located at coordinates 13x13 (the centre of the game board) and thus, this is your starting point. In the beginning, you have four directions available until the probable movement in any case would cause the board boundary to be surpassed or your path basically becomes an obstacle. The neighbouring digit represents the number of steps � if all conditions are met and you decide to go there, you are obviously away that number of steps from your previous position and gain the same amount of points. The game finishes once no movements are possible while in order to appear in the high score list, you must reach at least the amount of points the fifth person has. Good luck!

Tip: Right after you launch Cypher Walker, if you click the left mouse button on "Play" or press the Tab key and then Enter on your keyboard, you will be able to use it for making decisions with the following keyboard controls: I for going up, J for going left, L for going right and K for going down.

------------
Game options
------------

There, you can change sounds that are played during the game, such as when a new game is started, a movement is made, the game is lost or the player qualified to the top 5 of high scores. To mute any of them, just clear its corresponding text box. You can also choose another language.
